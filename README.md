# 车贷-回租模式 框架说明

文档在线版：https://gitee.com/git-fdjava/document_display_project/blob/master/README.md

## 描述
该框架由 Spring Boot 2.0 + mybatis-plus + mybatis 3.4.6 + druid 开发

## 开发说明
### 接口层(car-loan-api)  
基本代码路径:com.rskj99.carloan.api.transservice  

一、在包transservice下根据transcode创建对应Service(M911001_Service)  
二、其中service要实现TransService接口并加上@Service注解  
三、在包vo下创建对应的请求VO对象及响应VO对象(R_M911001、T_M911001)  
四、运行 CarLoanApiApplication 的 main 方法直接使用 Postman 测试即可  
PS：参考示例 M911001_Service  

### 服务层(car-loan-service)

基本代码路径:com.rskj99.carloan.api  
正常情况下不需要额外开发，直接使用test目录下CodeGeneratorTest类生成对应的Service和Mapper即可  
这里需要注意的代码生成器生成出来的entity需要放到car-loan-entity模块下  
PS：如果需要对Service进行单元测试，可参考test目录下的OrderInfoServiceTest


## 模块说明
car-loan-api - 对外接口层(tansservice)  
car-loan-entity - 实体层  
car-loan-service - 服务层(提供Service和mapper)  
common-db - 数据库底层依赖  
common-framework - 公共框架封装(目前提供Exception的封装及基础Utils)  
rscore-api-starter - 融数核心接口依赖封装  

## 依赖类库说明
**mybatis-plus 3.0**   
mybatis插件 提供 各类封装好的CRUD方法   
文档：http://mp.baomidou.com/#/  
这里需要注意一个点，目前mybatis-plus提供的文档为2.3，3.0的文档在月中之后才会释放

**druid 1.1.10**  
数据库连接池，性能优异，自带SQL监控  
文档：https://github.com/alibaba/druid/wiki/%E9%A6%96%E9%A1%B5

**hutool 4.1.6**  
优秀的工具类库，基本能提供所有在日常开发中使用到的工具方法  
强烈建议日常开发中直接使用该工具库，如无必要可以不用自己额外封装  
文档：http://hutool.mydoc.io/

**lombok**  
使用注解减少java中大量的重复代码编写生成  
注意，需要为IDE安装插件，请参考下面的插件安装文档  
插件安装文档：https://www.cnblogs.com/shindo/p/7550790.html

